//
//  TimelyBaseView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct TimelyBaseView: View {
    var body: some View {
        TaskPagerView()
    }
}

struct TimelyBaseView_Previews: PreviewProvider {
    static var previews: some View {
        TimelyBaseView()
    }
}
