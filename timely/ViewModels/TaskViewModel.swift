//
//  TaskViewModel.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import Foundation
import SwiftUI

public class TaskViewModel: Identifiable {
    public let id = UUID().uuidString
    public let viewName: String
    public let backgroundColor = Color.random
    public let duration: Int
    public var started = false
    public var ended = false
    private var timer = Timer()
    
    init(viewName: String, duration: Int) {
        self.viewName = viewName
        self.duration = duration
    }
}

public extension Color {
    static var random: Color {
        return Color(red: .random(in: 0...0.45),
                     green: .random(in: 0...0.45),
                     blue: .random(in: 0...0.45))
    }
}
