//
//  timelyApp.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

@main
struct timelyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
