//
//  ContentView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TimelyBaseView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
