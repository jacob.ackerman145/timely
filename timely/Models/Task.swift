//
//  Task.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import Foundation

struct Task {
    var duration: Int
    var start: Date?
    var end: Date?
    var wasSkipped: Bool = false
    var isComplete: Bool = false
    var name: String
}
