//
//  PageView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct PageView: View {
    let taskVM: TaskViewModel
    
    var body: some View {
        ZStack {
            LinearGradient(gradient:
                            Gradient(stops: [
                                        .init(color: taskVM.backgroundColor.opacity(0.8), location: 0),
                                        .init(color: Color.white, location: 0.9)]),
                           startPoint: .top,
                           endPoint: .bottom)
                .edgesIgnoringSafeArea(.all)
            
            GeometryReader { geometry in
                VStack(spacing: 12.0) {
                    Spacer()
                    
                    Text(self.taskVM.viewName.capitalized)
                        .font(.title)
                        .colorInvert()
                    
                    Group {
                        Text(self.taskVM.viewName)
                        Text(self.taskVM.id)
                    }
                    .frame(width: geometry.size.width - 24.0,
                           height: geometry.size.height * 0.3)
                    .background(self.taskVM.backgroundColor.opacity(0.9))
                    .foregroundColor(.white)
                    .cornerRadius(12)
                    .padding(EdgeInsets(top: 12.0,
                                        leading: 12.0,
                                        bottom: 12.0,
                                        trailing: 12.0))
                    
                    Spacer()
                }
            }
        }
    }
}

struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(taskVM: TaskViewModel(viewName: "test", duration: 60))
    }
}
