//
//  TaskView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct TaskView: View {
    let tasks: [TaskViewModel]
    
    @Binding var index: Int
    @State private var offset: CGFloat = 0
    @State private var isUserSwiping: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .center, spacing: 0) {
                    ForEach(self.tasks, id: \.id) {
                        tasksViewModel in
                        PageView(taskVM: tasksViewModel)
                            .frame(width: geometry.size.width,
                                   height: geometry.size.height)
                    }
                }
            }
            .content
            .offset(x: self.isUserSwiping ? self.offset : CGFloat(self.index) * -geometry.size.width)
            .frame(width: geometry.size.width, alignment: .leading)
            .gesture(
                DragGesture()
                    .onChanged({ value in
                        self.isUserSwiping = true
                        self.offset = value.translation.width + -geometry.size.width * CGFloat(self.index)
                    })
                    .onEnded({ value in
                        if value.predictedEndTranslation.width < geometry.size.width / 2, self.index < self.tasks.count - 1 {
                            self.index += 1
                        }
                        if value.predictedEndTranslation.width > geometry.size.width / 2, self.index > 0 {
                            self.index -= 1
                        }
                        withAnimation {
                            self.isUserSwiping = false
                        }
                    })
            )
        }
    }
}

struct TaskView_Previews: PreviewProvider {
    static var previews: some View {
        let task1 = TaskViewModel(viewName: "task1", duration: 60)
        let task2 = TaskViewModel(viewName: "task2", duration: 90)
        return TaskView(tasks: [task1, task2], index: .constant(0))
    }
}
