//
//  PagerCircleView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct PagerCircleView: View {
    @Binding var isSelected: Bool
    let action: () -> Void
    
    var body: some View {
        Button(action: {
            self.action()
        }) {
            Circle()
                .frame(width: 12, height: 12)
                .foregroundColor(.white)
                .overlay(GeometryReader { geometry in
                    Circle()
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .foregroundColor(self.isSelected ? .black : Color.gray.opacity(0.65))
                })
        }
    }
}

struct PagerCircleView_Previews: PreviewProvider {
    static var previews: some View {
        PagerCircleView(isSelected: .constant(false), action: {})
    }
}
