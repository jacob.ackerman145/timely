//
//  TaskPagerView.swift
//  timely
//
//  Created by Jacob Ackerman on 6/24/20.
//

import SwiftUI

struct TaskPagerView: View {
    let tasks = [
        TaskViewModel(viewName: "task1", duration: 60),
        TaskViewModel(viewName: "task2", duration: 90),
        TaskViewModel(viewName: "task3", duration: 120)
    ]
    
    @State private var index: Int = 0
    
    var body: some View {
        ZStack(alignment: .bottom) {
            TaskView(tasks: self.tasks, index: self.$index)
            HStack(spacing: 8) {
                ForEach(0..<self.tasks.count) { index in
                    PagerCircleView(isSelected: Binding<Bool>(get: {
                        self.index == index
                    }, set: { _ in })) {
                        withAnimation {
                            self.index = index
                        }
                    }
                }
            }
            .padding(.bottom, 30)
        }
    }
}

struct TaskPagerView_Previews: PreviewProvider {
    static var previews: some View {
        TaskPagerView()
    }
}
